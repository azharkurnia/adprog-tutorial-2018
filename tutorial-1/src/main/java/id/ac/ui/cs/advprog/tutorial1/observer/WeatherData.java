package id.ac.ui.cs.advprog.tutorial1.observer;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class WeatherData extends Observable {
    private ArrayList observers;
    private float temperature;
    private float humidity;
    private float pressure;

    public WeatherData() {
        observers = new ArrayList();
    }

    @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    public void notifyObservers() {
        ArrayList<Float> attributes = new ArrayList<>();
        attributes.add(this.temperature);
        attributes.add(this.humidity);
        attributes.add(this.pressure);
        for (int i = 0; i < observers.size(); i++) {
            Observer observer = (Observer)observers.get(i);
            observer.update(this,attributes);
        }
    }

    public void measurementsChanged() {
        setChanged();
        notifyObservers();
    }

    public void setMeasurements(float temperature, float humidity,
                                float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
    }

    public float getTemperature() {
        // TODO Complete me!
        return this.temperature;
    }

    public void setTemperature(float temperature) {
        // TODO Complete me!
        this.temperature = temperature;
        measurementsChanged();
    }

    public float getHumidity() {
        // TODO Complete me!
        return this.humidity;
    }

    public void setHumidity(float humidity) {
        // TODO Complete me!
        this.humidity = humidity;
        measurementsChanged();
    }

    public float getPressure() {
        // TODO Complete me!
        return this.pressure;
    }

    public void setPressure(float pressure) {
        // TODO Complete me!
        this.pressure = pressure;
        measurementsChanged();
    }
}
