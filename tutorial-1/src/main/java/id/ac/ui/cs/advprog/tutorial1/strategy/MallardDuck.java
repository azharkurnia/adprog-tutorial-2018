package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {

    // TODO Complete me!
    public MallardDuck() {
        setQuackBehavior(new Quack());
        setFlyBehavior(new FlyWithWings());
    }

    @Override
    public void display() {
        System.out.println("This is mallard duck");
    }
}
