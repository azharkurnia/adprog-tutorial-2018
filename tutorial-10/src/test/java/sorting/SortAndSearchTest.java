package sorting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class SortAndSearchTest {

    private static long oneSecondInNano;
    private int[] arr1;
    private int[] arr2;
    private int[] array1;
    private int[] array2;
    private int searchInt;
    private int randomInt;

    @Before
    public void setUp() throws IOException {
        oneSecondInNano = 100000000;
        arr1 = Main.convertInputFileToArray();
        arr2 = arr1.clone();
        searchInt = 1629;
        randomInt = -678;
        array1 = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        array2 = new int[]{4, 5, 7, 6, 8, 9, 1, 3, 2, 10};

    }

    @Test
    public void testMainMethod() {
        assertTrue(mainChecker());
    }

    // Method to check the working main class
    private boolean mainChecker() {

        boolean found = true;
        try {
            Main.main(null);
        } catch (Exception e) {
            found = false;
        } finally {
            return found;
        }
    }


    @Test
    public void testFindNotFoundInt() {
        assertEquals(-1, Finder.slowSearch(arr1, randomInt));
        assertEquals(-1, Finder.fastSearch(arr1, randomInt));
        int[] arrayIntSort = Sorter.fastSort(arr1);
        assertEquals(-1, Finder.fastSearch(arrayIntSort, randomInt));
    }

    @Test
    public void slowSortTest() {
        Sorter.slowSort(array2);
        for (int i = 0; i < array2.length; i++) {
            assertEquals(array2[i], array1[i]);
        }
    }

}
