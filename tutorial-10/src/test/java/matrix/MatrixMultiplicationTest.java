package matrix;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class MatrixMultiplicationTest {
    private static String genericMatrixPath = "plainTextDirectory/input/matrixProblem";
    private static String pathFileMatrix1 = genericMatrixPath + "A/matrixProblemSet1.txt";
    private static String pathFileMatrix2 = genericMatrixPath + "A/matrixProblemSet2.txt";

    private double[][] thenonSquareMatrix;
    private double[][] matrix1;
    private double[][] matrix2;

    @Before
    public void setUp() throws IOException {
        thenonSquareMatrix = new double[][]{{45, 67, 89, 100, 111}, {112, 56, 66, 88, 32}};
        matrix1 = Main.convertInputFileToMatrix(pathFileMatrix1, 50, 50);
        matrix2 = Main.convertInputFileToMatrix(pathFileMatrix2, 50, 50);
    }

    @Test
    public void testMainClass() {
        assertTrue(mainClassChecker());
    }

    // Helper method for check the main class
    private static boolean mainClassChecker() {
        boolean success = true;
        try {
            Main.main(null);
        } catch (Exception e) {
            success = false;
        } finally {
            return success;
        }
    }

    @Test
    public void illegalMultiplyTester() {
        boolean isIllegal = false;
        try {
            MatrixOperation.basicMultiplicationAlgorithm(thenonSquareMatrix, thenonSquareMatrix);
        } catch (InvalidMatrixSizeForMultiplicationException e) {
            isIllegal = true;
        } finally {
            assertTrue(isIllegal);
        }
    }


}
