package sorting;

public class Finder {


    /**
     * Some searching algorithm that possibly the slowest algorithm.
     * This algorithm can search a value irregardless of whether the sequence already sorted or not.
     *
     * @param arrOfInt      is a sequence of integer.
     * @param searchedValue value that need to be searched inside the sequence.
     * @return -1 if there are no such value inside the sequence, else return searchedValue.
     */
    public static int slowSearch(int[] arrOfInt, int searchedValue) {
        int returnValue = -1;

        for (int element : arrOfInt) {
            if (element == searchedValue) {
                returnValue = element;
            }
        }

        return returnValue;
    }

    public static int fastSearch(int[] a, int key) {
        return search(key, a, 0, a.length);
    }

    private static int search(int key, int[] a, int lo, int hi) {
        if (hi <= lo) {
            return -1;
        }
        int mid = lo + (hi - lo) / 2;
        int cmp = a[mid] > key ? 1 : -1;
        if (cmp > 0) {
            return search(key, a, lo, mid);
        } else if (cmp < 0) {
            return search(key, a, mid + 1, hi);
        } else {
            return mid;
        }
    }
}
