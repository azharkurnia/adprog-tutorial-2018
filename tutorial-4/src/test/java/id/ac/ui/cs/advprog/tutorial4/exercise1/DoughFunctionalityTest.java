package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.NoCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DoughFunctionalityTest {

    private Dough noCrustDough;
    private Dough thinCrustDough;
    private Dough thickCrustDough;

    @Before
    public void setUp() throws Exception {
        noCrustDough = new NoCrustDough();
        thinCrustDough= new ThinCrustDough();
        thickCrustDough= new ThickCrustDough();

    }

    @Test
    public void testClamsOutput(){
        assertEquals("No Crust Dough",noCrustDough.toString());
        assertEquals("ThickCrust style extra thick crust dough",thickCrustDough.toString());
        assertEquals("Thin Crust Dough",thinCrustDough.toString());
    }

}
